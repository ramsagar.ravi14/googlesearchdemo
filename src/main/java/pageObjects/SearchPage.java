package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {

	public WebDriver driver;

	By Searchbox = By.name("q");

	public SearchPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getSearchbox() {
		return driver.findElement(Searchbox);
	}

	public void setSearchbox(String SearchText) {
		driver.findElement(Searchbox).sendKeys(SearchText);
	}

}
