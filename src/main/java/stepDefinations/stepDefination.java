package stepDefinations;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//import Cucumberfrm.Googlesearchproj.base;
import resources.base;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import pageObjects.SearchPage;

public class stepDefination extends base {

	private base bobj;

	public stepDefination(base bobj) {
		this.bobj = bobj;
	}

	public static Logger Log = LogManager.getLogger(base.class.getName());

	@Given("^I open \"([^\"]*)\" website$")
	public void i_open_something_website(String strArg1) throws Throwable {

		bobj.driver.get("https://www.google.com");
		assertEquals("https://www.google.com/", bobj.driver.getCurrentUrl());
		Log.info("Google.com is open");
	}

	@When("^I search for \"([^\"]*)\" in google$")
	public void i_search_for_something_in_google(String searchkeword) throws Throwable {

		SearchPage s = new SearchPage(bobj.driver);
		s.setSearchbox(searchkeword + "\n"); // \n is used to perform Enter Key action
		Log.info("Clarivate Analytics phrase is inserted into Google Searchbox and Enter Key action performed");
	}

	@Then("^I Verify on \"([^\"]*)\" is the first link$")
	public void i_verify_on_something_is_the_first_link(String strArg1) throws Throwable {
		List<WebElement> findElements = bobj.driver.findElements(By.xpath("//*[@id='rso']/div[1]/div/div/link"));
		Log.info("First element in google search is " + findElements.get(0).getAttribute("href"));
		assertEquals(strArg1, findElements.get(0).getAttribute("href"));
		bobj.driver.close();
	}

}