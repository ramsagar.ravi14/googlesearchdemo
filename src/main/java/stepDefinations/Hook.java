package stepDefinations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

/*import Cucumberfrm.Googlesearchproj.base;*/
import resources.base;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hook extends base {

	private base bobj;

	public Hook(base bobj) {
		this.bobj = bobj;
	}

	@Before
	public void initializeDriver() throws IOException {
		WebDriverManager.chromedriver().setup();
		FileInputStream fis;
		try {
			fis = new FileInputStream(".\\src\\main\\resources\\configdata.properties");
			prop.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String browserName = prop.getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {
			bobj.driver = new ChromeDriver();
			bobj.driver.manage().window().maximize();
			bobj.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

	}

	@After
	public void tearDown(Scenario scenario) {
		if (scenario.isFailed()) {
			// Take a screenshot...
			final byte[] screenshot = ((TakesScreenshot) bobj.driver).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", scenario.getName());
		}
		bobj.driver.quit();
	}

}
