package stepDefinations;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

//import Cucumberfrm.Googlesearchproj.base;
import resources.base;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import pageObjects.SearchPage;

public class Squareroot {

	private base bobj;

	public Squareroot(base bobj) {
		this.bobj = bobj;
	}

	public static Logger Log = LogManager.getLogger(base.class.getName());

	@Given("^I open Windows Caluculator app$")
	public void i_open_windows_caluculator_app() throws Throwable {

		DesktopOptions option = new DesktopOptions();
		option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
		bobj.wdriver = new WiniumDriver(new URL("http://localhost:9999"), option);
		Log.info("Caluculator is open");
	}

	@When("^I find Square root of \"([^\"]*)\" in caluculator$")
	public void i_find_square_root_of_something_in_caluculator(String strArg1) throws Throwable {

		char[] num = strArg1.toCharArray();
		for (int i = 0; i < num.length; i++) {
			
			switch (num[i]) {
			// Case statements
			case '1':
				bobj.wdriver.findElement(By.name("One")).click();
				break;
			case '2':
				bobj.wdriver.findElement(By.name("Two")).click();
				break;
			case '3':
				bobj.wdriver.findElement(By.name("Three")).click();
				break;
			case '4':
				bobj.wdriver.findElement(By.name("Four")).click();
				break;
			case '5':
				bobj.wdriver.findElement(By.name("Five")).click();
				break;
			case '6':
				bobj.wdriver.findElement(By.name("Six")).click();
				break;
			case '7':
				bobj.wdriver.findElement(By.name("Seven")).click();
				break;
			case '8':
				bobj.wdriver.findElement(By.name("Eight")).click();
				break;
			case '9':
				bobj.wdriver.findElement(By.name("Nine")).click();
				break;
			}			
		}
		Log.info(strArg1 +" is Entered");
		bobj.wdriver.findElement(By.id("squareRootButton")).click();
		Log.info("Squareroot of " + strArg1 + " is Caluculated");

	}

	@Then("^I Verify the result \"([^\"]*)\" in caluculator$")
	public void i_verify_the_result_something_in_caluculator(String strArg1) throws Throwable {

		String result = bobj.wdriver.findElement(By.id("CalculatorResults")).getAttribute("Name");
		Log.info(result);
		String[] sqrtvalue = result.split(" ");
		String sqrtofnum = sqrtvalue[2];
		assertEquals(strArg1, sqrtofnum);
		Log.info("Result is verified");
		bobj.wdriver.findElement(By.id("Close")).click();
		Log.info("Caluculator is closed");
	}

}